# Controle de temperatura

Este repositório destina-se ao desenvolvimento da parte eletrônica do projeto Silo, visando implementar um sistema de controle de temperatura capaz de garantir a conservação adequada dos grãos armazenados.

# Estrutura do Repositório

O repositório está organizado da seguinte forma:

```bash
esquemáticos         ->  Esquemáticos do sistema eletrônico
diagramas            ->  Diagramas do sistema eletrônico
sistema-de-controle  ->  Firmware do projeto
```
# Resumo do Projeto

## 1. Identificação do problema

Especifica os requisitos de eletrônica do projeto "Silo", fornecendo aos envolvidos as informações necessárias para seu desenvolvimento e implementação, assim como para a realização dos testes funcionais do sistema. 

## 3.	Convenções 

### 3.1 Identificação de requisitos

Por convenção, a referência a requisitos é feita através do nome da subseção na qual eles estão descritos, seguidos do identificador do requisito, de acordo com o modelo:

*[nome da subseção/identificador do requisito]*

Os requisitos serão identificados com um identificador único, com numeração iniciada em **[RF001]** para um *requisito funcional*, **[NF001]** para um *requisito não funcional*.

### 3.2 Propriedades dos requisitos

Para estabelecer a prioridade dos requisitos, nas seções 4, e 5, foram adotadas as denominações “essencial”, “importante” e “desejável”, tal que:

Essencial é o requisito sem o qual o sistema não entra em funcionamento. Requisitos essenciais são aqueles imprescindíveis, que devem ser implementados impreterivelmente.

Importante é o requisito sem o qual o sistema entra em funcionamento, mas de forma não satisfatória. Requisitos importantes devem ser implementados, mas, se não forem, o sistema poderá ser implementado e usado normalmente.

Desejável é o requisito que não compromete as funcionalidades básicas do sistema, isto é, o sistema pode funcionar de forma satisfatória sem ele. Requisitos desejáveis podem ser deixados para versões posteriores do sistema, caso não haja tempo hábil para implementação deles nesta etapa.

## 4.	 Requisitos funcionais

### 4.1 [RF01] Detectar variação de temperatura

O sistema deve ser capaz de medir a temperatura e emissão de C02 dentro do silo.

**Prioridade**: Essencial

### 4.2 [RF02] Controle de temperatura

O sistema deve possuir um microcontrolador capaz de processar as informações recebidas e fazer o controle de temperatura do silo de forma automática.

**Prioridade**: Essencial

### 4.3 [RF03] Envio de dados e recebimento de dados

O sistema deve ser capaz de se comunicar com um servidor de modo a enviar e receber dados.

**Prioridade**: Essencial

### 4.3 [RF04] Automatização on/off do sistema eletrônico

O sistema eletrônico deve ser capaz de ligar e desligar o sistema de temperatura de forma automatizada. 

**Prioridade**: Desejável
	

## 5.	Requisitos não funcionais

### 5.1 [NF01] Proteção contra temperatura

O sistema eletrônico deve ser projetado para resistir a temperaturas elevadas, garantindo a integridade dos componentes internos mesmo em condições de calor elevado.

**Prioridade**: Essencial

### 5.2 [NF02] Controle adequado da temperatura operacional do silo

O sistema deve manter a temperatura dos grãos dentro da faixa operacional recomendada, evitando que ultrapassem os limites inferiores e superiores que poderiam resultar em danos aos grãos.

**Prioridade**: Essencial

### 5.2 [NF03] Envio de dados em tempo real
O sistema deve ser capaz de enviar os dados de temperatura e de emissão de CO2 para o servidor a cada 10 minutos.

**Prioridade**: Importante


3 [RN04] USO DO PROTOCOLO MQTT

O sistema deve se comunicar com o servidor via protocolo MQTT.

**Prioridade**: Importante


# 7.	Descrição de hardware

## 7.1 BOOM(Bill of materials)

[Link com a BOOM no excel](https://1drv.ms/x/s!Anm8H1ITOcClipgqVo-yRAalRg3AlQ?e=Bn4Vwz)

| **ID**    | **NOME**                  | **DESCRIÇÃO**                                                                                                                    | **QTD.** | **PREÇO** | **FRETE** | **DESCONTO** | **ADQUIRIDOS** | **TOTAL** |
|-----------|---------------------------|----------------------------------------------------------------------------------------------------------------------------------|----------|-------------|-------------|----------------|------------------|-------------|
| **E01**   | ESP32                     | Módulo WiFi NodeMCU ESP-32 + Bluetooth                                                                                           | 1        |  R$ 83,90   |  R$ -       |  R$ 83,90      |  SIM             |  R$ -       |
| **E02**   | Módulo Dimmer MC-8A 220V  | O Módulo Dimmer Arduino e ESP32 MC-8A 220V é um equipamento eletrônico que permite realizar o controle da quantidade de energia. | 1        |  R$ 46,30   |  R$ 30,04   |  R$ -          |  SIM             |  R$ 76,34   |
| **E03**   | Módulo CCS811             | Módulo Detector de Dióxido de Carbono C02                                                                                        | 1        |  R$ 80,00   |  R$ 34,62   |  R$ 20,00      |  SIM             |  R$ 94,62   |
| **E04**   | Triac BT139 800E          | Triac BT139 800E                                                                                                                 | 1        |  R$ 3,00    |  R$ 34,62   |  R$ 20,00      |  SIM             |  R$ 17,62   |
| **E05**   | Mini Fonte AC/DC HLK-PM01 | Mini Fonte 100/240VAC para 5VDC (HLK-PM01)                                                                                       | 1        |  R$ 33,99   |  R$ -       |  R$ -          |  NÃO             |  R$ 33,99   |
| **E06**   | Relé 5V                   | Módulo Relé 5V/2 canais                                                                                                          | 1        |  R$ 15,90   |  R$ -       |  R$ -          |  NÃO             |  R$ 15,90   |
| **E07**   | LM35DZ                    | Sensor de Temperatura LM35                                                                                                       | 3        |  R$ 19,82   |  R$ -       |  R$ -          |  NÃO             |  R$ 59,46   |
| **E08**   | Placa de Fenolite 10x10cm | Chapa de Fenolite 10x10cm                                                                                                        | 1        |  R$ 9,00    |  R$ -       |  R$ -          |  NÃO             |  R$ 9,00    |
| **E09**   | Conector Borne            | Conector Borne KRT-2T Passo 5mm                                                                                                  | 4        |  R$ 0,89    |  R$ -       |  R$ -          |  NÃO             |  R$ 3,56    |
| **Total** |                           |                                                                                                                                  |          |             |             |                |  R$ 188,58       |  R$ 310,49  |


# 8.	Diagramas

# 8.1	Diagrama de blocos

![Diagrama de blocos dos subsistemas](./diagramas/Diagrama_de_blocos_sistemas.drawio.png)

# 8.2 Diagrama de Alimentação dos Circuitos Eletrônicos

![Diagrama de blocos dos subsistemas](./diagramas/Diagrama_de_alimentacao.drawio.png)

# 9.	Esquemáticos

O projeto eletrônico do silo foi dividido em quatro subsistemas principais: Alimentação, Sensoriamento, Microcontrolador e Acionamento de resistência. Na figura a seguir, é apresentado o esquemático geral do sistema eletrônico desenvolvido no EasyEDA.

![Esquemático eletrônico do Projeto Silo](./esquematicos/Esquematico.png)

# 10	Descrição textual 

**Arquitetura do Subsistema de Eletrônica**

**Descrição geral do sistema eletrônico**

O sistema eletrônico é responsável pelo gerenciamento e acionamento dos subsistemas que compõe o projeto silo. Ao todo, foram definidos quatro subsistemas que irão compor o projeto eletrônico mostrados no diagrama de blocos na Figura x, as conexões sinalizam como os subsistemas interagem entre si e o mundo externo.

**Subsistema de Sensoriamento:** O subsistema de sensoriamento foi desenvolvido para realizar medições periódicas da temperatura de saída da haste e da temperatura interna do silo. Seu funcionamento se baseia em um controlador PID (Integral, Proporcional e Derivativo). O subsistema também faz o monitoramento de respiração de grãos usando o monitoramento de C02. 

**Subsistema de Controle de Potência:** O subsistema de acionamento de resistência tem como objetivo fazer o controle de potência dissipada no resistor de potência do circuito, de modo a garantir que a temperatura seja controlada pelo microcontrolador em conjunto com o subsistema de sensoriamento.

**Subsistema de Controle:** O subsistema do microcontrolador é responsável por receber informações provenientes dos sensores e fazer o acionamento da resistência conforme os padrões estabelecidos para garantir a saúde dos grãos. O subsistema também faz os cálculos necessários, bem como

**Subsistema Alimentação:** O subsistema de alimentação tem como objetivo fornecer as tensões e correntes adequadas para todos os outros subsistemas do projeto. Seu funcionamento se baseia em um regulador de tenção STEP-DOWN 12DV/5DC .   

**Componentes selecionados para o projeto**

**Microcontrolador ESP32**
O microcontrolador ESP32 foi selecionado para integrar o sistema eletrônico do silo, pois possui comunicação Wi-Fi e Bluetooth em um único chip, permitindo interfacear e enviar dados para o servidor externo. Esse micocontrolador também possui processador dual-core, sensores embutidos, pinos de entrada e saída, como GPIOs, I2C, SPI, UART, ADC e DAC. Essas características permitem a realizações das operações no sistema, o monitoramento de dados analógicos e o processamento de informações para o controle da temperatura no silo.

**Sensor de temperatura LM35**
O sensor de temperatura LM35 é um sensor de baixo custo e baixo consumo de energia. Ele foi escolhido por permitir o monitoramento da temperatura com uma precisão de ±5/4ºC, medir temperaturas na faixa de -55ºC à 150º e possuir uma sensibilidade de 10mV/1ºC. Essas características fazem com que o subsistema de sensoriamento possa medir as temperaturas de saída da haste e do silo a fim de informar ao microcontrolador os parâmetros adequados para a criação do PID do sistema. 

**Sensor de qualidade do ar CCS811 CJMCU-811**
O sensor de qualidade do ar CCS811 CJMCU-811 foi escolhido para detectar dióxido de carbono (CO2) e compostos orgânicos voláteis (COV) no ambiente interno do silo. Com uma faixa de medição de CO2 de 400 ppm a 32768 ppm, permitindo a monitoramento da respiração dos grãos. Sua utilização frequente em ambientes internos o torna adequado para o ambiente dos silos, onde variações irregulares de CO2 podem indicar condições anormais nos grãos armazenados.

**Modulo Dimmer ESP32 MC-8A 220V**
O modúlo dimer é um modulo que faz o controle de potência em cargas resistivas, no caso do projeto silo, a resistência de potência. Ele é um modulo adaptado para para chavear a corrente alternada, onde tem a capacidade de dimerizar resistência atraves de microcontroladores. Ele foi escolhido por conta de ser um módulo de baixo custo e fácil utilização. 

# 11.	Descrição de firmware

# Controle de Versão

|Nome | Data da ultima Modificação |
|-----|---------------------|
|Luiz | 26/04/2024          |


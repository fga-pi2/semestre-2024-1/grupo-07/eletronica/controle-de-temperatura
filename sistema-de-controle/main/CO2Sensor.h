#ifndef CO2_SENSOR_H
#define CO2_SENSOR_H

class CO2Sensor {
private:
    int pin; 
    float concentration;

public:
    CO2Sensor();
    void initializeSensor(int pin);
    float getCO2Concentration();
    void calibrate();
};

#endif

# Ponto de Controle

Diagramas elétricos e eletrônicos do sistema, sendo composto por diagramas unifilares/trifilares (com os dispositivos de proteção, seccionamento, seção de fios, etc.) de sistemas de alimentação, diagramas esquemáticos de circuitos eletrônicos (com identificação dos componentes eletrônicos que serão utilizados nos circuitos), diagramas detalhando barramentos de alimentação dos circuitos eletrônicos (ou seja, trata-se da interface entre sistemas de alimentação e circuitos eletrônicos), diagramas com detalhes de lógicas e protocolos de comunicação entre elementos (microcontrolador com microcontrolador, microcontrolador e sensor, microcontrolador e atuador, microcontrolador e software, etc);

# Primeiro Esquemático do Projeto

![Esquemático eletrônico do Projeto Silo](./Esquematico.png)


|Nome | Data da Modificação |
|-----|---------------------|
|Luiz | 24/04/2024          |

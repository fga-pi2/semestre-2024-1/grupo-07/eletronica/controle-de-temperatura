#ifndef RELAY_SYSTEM_H
#define RELAY_SYSTEM_H

class Relay_system{
private:
    int pin;
public:
    Relay_system(int pin);
    int powerOn();
    int powerOff();
};

#endif
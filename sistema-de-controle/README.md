# Firmware do Projeto da Haste

O código neste repositório foi desenvolvido e implantado na ESP32 por meio de uma parceria entre os estudantes dos grupos de Software e Eletrônica.

O firmware realiza a leitura dos sensores de CO2 e temperatura e envia esses dados para o sistema de software, por meio do protocolo MQTT. Além disso, ele controla o acionamento da resistência e do motor do ventilador.

## Como executar o firmware

### Pré-requisitos de Hardware

- ESP32.
- Fonte de alimentação para a ESP32.
- Cabo USB (para realizar a compilação do código, é necessário que o cabo seja de dados).
- Sensores de temperatura LM35.
- Sensor de CO2 CCS811.

Conecte os sensores na GPIO da ESP32. Depois, alimente a ESP32. 
Para compilar, é necessário que a alimentação seja realizada com um cabo USB de dados. A ESP32, além disso, deve ser conectada num computador.

### Pré-requisitos de Software

- Arduino IDE (necessária para instalar as libs e compilar).
- Bibliotecas necessarias do Arduino (elas estão especificadas logo abaixo).
- Desligar o firewall do computador para evitar problemas na comunicação MQTT.
- Arquivo WifiCredentials.h, no diretório `main`. Este arquivo será especificado mais abaixo.

#### Instalações necessárias

Siga o roteiro para instalar o software necessário e compilar o programa no microcontrolador:

1. Baixe o [arduinoIDE](https://www.arduino.cc/en/software). Se for usuario de sistemas \*nix, não baixe as versões disponíveis nos empacotadores snap, apt, entre outros. 
2. Se for usuario de unix, transforme o arquivo em um executável e em seguida o execute.
3. No canto superior esquedo da tela da IDE, acesse `file->preferences`.
4. No input `Additional boards manager URLs`, cole o seguinte URL: `https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json`. Nesta etapa, estamos instalando o software necessário para compilar o código de arduino na ESP32.

![imgBoards](./imagens/adittionalBoards.png)

5. Clique em OK. A IDE, neste momento, iniciará o download do software da ESP32.
6. Após o download, conecte a ESP32 no seu computador.
7. Abra o `Boards manager`. Para isso, clique no ícone que está na parte da esquerda da IDE. É o segundo ícone de cima para baixo.

![imgManager](./imagens/boardsManager.png)

8. Procure por `esp32`, e instale a board `esp32 by Espressif Systems`.
9. Após a instalação, selecione a board. Para isso, clique no drop-down que está na parte de cima da IDE, e selecione a opção `Select other board and port...`.

![dropdown](./imagens/dropdown.png)

10. No campo de busca, escreva `doit`, e selecione a opção `DOIT ESP32 DEVKIT V1`.

![doit](./imagens/doit.png)

11. Agora, vamos instalar as libs que estamos utilizando. Logo abaixo do ícone das boards, está o ícone das libs. Clique nele.
12. Para instalar uma lib, basta selecionar ela e clicar em `install`. 

![library](./imagens/library.png)

13. Busque por `arduinojson`, e instale a lib `ArduinoJson by Benoit Blanchon`.
14. Busque por `PubSubClient`, e instale a lib `PubSubClient by Nick O'Leary`.
15. Busque por `DallasTemperature`, e instale a lib `DallasTemperature by Miles Burton`.
16. Busque por `OneWire`, e instale a lib `OneWire by Jim Studt`.
17. Busque por `ccs811`, e instale a lib `Adafruit CCS811 Library by Adafruit`.
18. Busque por `pid_v1_bc`, e instale a lib `PID_v1_bc by David Forrest`.
19. Todo o necessário está instalado. Agora, para compilar, clique no ícone da seta para a direita.

![buton](./imagens/button.png)

### Escolha das libraries

- A lib `ArduinoJson` foi escolhida para facilitar a conversão dos dados dos sensores para o formato JSON.
- A lib `PubSubClient` foi escolhida por ser uma recomendada pela comunidade para uso do protocolo MQTT.
- A lib `Adafruit CCS811` foi escolhida por ser recomendada para o uso do sensor CCS811.
- A lib `DallasTemperature` foi escolhida para ser usada no sensor de temperatura DS18B20. 
- A lib `OneWire` é necessária como dependência da lib `DallasTemperature`.
- A lib `PID_v1_bc` é necessária para o controle da resistência usando o algoritmo PID.

### O arquivo WifiCredentials.h

Este arquivo, que está na pasta `main`, é ignorado pelo `gitignore`. Isto porque ele é utilizado como um arquivo com variáveis de ambiente.
Cada chave e valor está numa macro de c++.

Exemplo do conteúdo deste arquivo:

    #define ssid "nome da rede wifi" //string
    #define password "senha da rede wifi" //string
    #define brokerURL "ip da maquina na rede Wi-fi" //string
    #define brokerPort 1883 //porta do broker. Este valor é um inteiro.     
    #define mqtt_topic "silo-sensors" //topico utilizado 

### Possíveis problemas durante a configuração

Durante o processo de configuração do seu ambiente para executar o embarcado de do projeto da haste pode-se encontrar problemas quanto a permissao de leitura e escrita da porta USB.

Caso isso ocorra, pode ser resolvido dando a [devida permissao na porta USB](https://stackoverflow.com/questions/73923341/unable-to-flash-esp32-the-port-doesnt-exist) do seu computador. Se estiver utilizando um sistema operacional unix, utilize o seguinte comando: 

    $ sudo adduser <username> dialout

    $ sudo chmod a+rw /dev/ttyUSB0


#include "Arduino.h"
#include "CO2Sensor.h"

CO2Sensor::CO2Sensor() {
    this->concentration = 0.0;
}

// converte o valor da entrada analógica para PPM
float getPPM(int raw_adc) {
    float voltage = raw_adc * (3.3 / 4095.0);
    float ratio = voltage / (3.3 - voltage);
    // Use os valores específicos da calibração do seu sensor
    float ppm = 116.6020682 * pow(ratio, -2.769034857);
    return ppm;
}

void CO2Sensor::initializeSensor(int pin) {
  this->pin = pin;
  pinMode(this->pin, INPUT);
  //this->calibrate();
}

void CO2Sensor::calibrate() {
  Serial.println("Calibrando o sensor de co2...");
  while(millis() < 5000 * 60) {
    analogRead(this->pin);
    Serial.println(this->getCO2Concentration());
  }
  Serial.println("Sensor calibrado!");
}

float CO2Sensor::getCO2Concentration() {
    int pinValue = 0;
    for(int i = 0; i < 10; i++) {
      pinValue += analogRead(this->pin);
      delay(200);
    }
    //this->concentration = getPPM(pinValue);

    this->concentration = pinValue / 10;
    return this->concentration;
}

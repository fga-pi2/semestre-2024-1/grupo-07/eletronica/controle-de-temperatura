// BAUD RATE
#define BAUD_RATE           9600

// pinos para teste
#define PIN_RELAY_1         5  
//#define PIN_RELAY_2         17 

// Temperature
#define PIN_TEMPERATURE1 14
#define PIN_TEMPERATURE2 35

// Dimmer
#define PIN_DIM             12
#define PIN_ZERO_CROSS      19

// CO2
#define MQ135_PIN 34

// type
struct Values {
   float temperature1;
   float temperature2;
   float co2;
   int resistence;
};

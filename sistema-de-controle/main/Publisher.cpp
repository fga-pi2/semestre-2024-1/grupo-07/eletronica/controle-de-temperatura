#include "Publisher.h"
#include "Arduino.h"
#include <WiFi.h>
#include <PubSubClient.h>
#include "WifiCredentials.h"
#include <ArduinoJson.h>
#include <iostream>
#include <sstream>

JsonDocument createMessage(char* type, float value) {
    JsonDocument doc;
    doc["type"] = type;
    doc["value"] = value;
    doc["timestamp"] = millis();
    doc["silo"] = "silo-1";

    return doc;
}

std::vector<int> Publisher::parseIP(const std::string& ip) {
    std::vector<int> result;
    std::stringstream ss(ip);
    std::string segment;

    while (std::getline(ss, segment, '.')) {
        result.push_back(std::stoi(segment));
    }

    return result;
}

void Publisher::connectToWifi(PubSubClient& client) {
    Serial.println("Conectando à rede wifi");
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("Conectado à rede ");
    Serial.print(ssid);
}

void Publisher::sendData(PubSubClient& client, Values &values) {
    Serial.println("Enviando dados ao servidor MQTT");

    JsonDocument message;
    
    message[0] = createMessage("temperature1", values.temperature1);
    message[1] = createMessage("temperature2", values.temperature2);
    message[2] = createMessage("co2", values.co2);
    message[3] = createMessage("resistence", values.resistence);


    char msg[2000];

    serializeJson(message, msg, sizeof(msg));

    //serializeJson(message, Serial);
    if(client.publish(mqtt_topic, msg)){
      Serial.println("mensagem publicada");
    }
    else{
      Serial.println("falha no envio da mensagem");
    }

  /*
    if (client.subscribe(mqtt_topic)) {
        Serial.println("Inscrito no tópico com sucesso");
    } else {
        Serial.println("Falha ao inscrever no tópico");
    }*/
}

void Publisher::reconnect(PubSubClient& client) {
  String fullBrokerURL = String(brokerURL) + ":" + String(brokerPort);

    while(!client.connected()) {
        if (client.connect("ESP32Client")) {
            Serial.println("Conectado ao broker MQTT");
        }
        else {
            Serial.println("Reconectando");
            Serial.print("Falha na conexão, estado: ");
            Serial.println(client.state());
            switch (client.state()) {
                case -4: Serial.println("MQTT_CONNECTION_TIMEOUT - O servidor não respondeu dentro do período de tempo esperado"); break;
                case -3: Serial.println("MQTT_CONNECTION_LOST - A conexão foi perdida"); break;
                case -2: Serial.println("MQTT_CONNECT_FAILED - A conexão falhou"); break;
                case -1: Serial.println("MQTT_DISCONNECTED - Desconectado"); break;
                case 1: Serial.println("MQTT_CONNECT_BAD_PROTOCOL - Versão do protocolo incorreta"); break;
                case 2: Serial.println("MQTT_CONNECT_BAD_CLIENT_ID - ID do cliente incorreto"); break;
                case 3: Serial.println("MQTT_CONNECT_UNAVAILABLE - Servidor indisponível"); break;
                case 4: Serial.println("MQTT_CONNECT_BAD_CREDENTIALS - Credenciais incorretas"); break;
                case 5: Serial.println("MQTT_CONNECT_UNAUTHORIZED - Não autorizado"); break;
                default: Serial.println("Estado desconhecido"); break;
            }
            delay(5000);
        }
    }
}

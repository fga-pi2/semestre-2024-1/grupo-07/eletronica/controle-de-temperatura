#include "Relay_system.h"
#include "Arduino.h"

Relay_system::Relay_system(int pin){
    this->pin = pin;
    pinMode(this->pin, OUTPUT);
}

int Relay_system::powerOn(){
    // the system turns on when set relay pin to LOW!
    digitalWrite(this->pin, LOW);  // I need to receive a flag "LOW OR HIGH" from the web server to automatically power on the relay
    return (1);
}

int Relay_system::powerOff(){
    // the system turns off when set relay pin to HIGH!
    digitalWrite(this->pin, HIGH);  // I need to receive a flag "LOW OR HIGH" from the web server to automatically power on the relay
    return (0);
}
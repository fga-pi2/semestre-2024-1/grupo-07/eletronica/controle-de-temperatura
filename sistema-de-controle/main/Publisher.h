#ifndef PUBLISHER_H
#define PUBLISHER_H

#include "config.h"
#include <WiFi.h>
#include <PubSubClient.h>
#include <string>
#include <vector>

class Publisher {
private:

public:
    void connectToWifi(PubSubClient& client);
    void sendData(PubSubClient& client, Values &values);
    void reconnect(PubSubClient& client);
    static std::vector<int> parseIP(const std::string& ip);
};

#endif 

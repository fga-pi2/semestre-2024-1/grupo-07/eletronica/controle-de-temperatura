#ifndef TemperatureSensor_h
#define TemperatureSensor_h

#include "Arduino.h"
#include "esp_adc_cal.h"

class TemperatureSensor {
  public:
    TemperatureSensor(int pin, int modeSensor);
    float getTemperature();
    float getSmoothedTemperature();  

  private:
    int pin;
    int modeSensor;
    //esp_adc_cal_characteristics_t *adc_chars;
    //uint32_t readADC_Cal(int ADC_Raw);
    //adc1_channel_t getADC1Channel(int pin);
    
    // média móvel
    //static const int bufferSize = 10;  
    //float temperatureBuffer[bufferSize];
    //int bufferIndex = 0;
    //bool bufferFilled = false;
};

#endif

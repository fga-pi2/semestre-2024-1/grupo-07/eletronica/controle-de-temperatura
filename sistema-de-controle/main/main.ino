#include <OneWire.h>
#include <DallasTemperature.h>
#include <PID_v1_bc.h>
#include "TemperatureSensor.h"
#include "CO2Sensor.h"
#include "Publisher.h"
#include "WifiCredentials.h"
#include "Relay_system.h"
#include "LiquidCrystal_I2C.h"

// N sei o que é isso (verificar)
void callback(char* topic, byte* payload, unsigned int length);

// Variáveis globais
bool mutexMQTT = false;
bool mutexPID = true;                                       
Values values = {0.0, 0.0, 0.0, 0};

volatile long intensidade = 0;                      // Potência na carga

double Setpoint, Input, Output;                     // Parâmetros do pid

int stateEnergy = 0;                                // Estado de energia 
int stateDimmer = 0;                                // Estado de Dimmer 
int lcdColumns = 16;                                // Qtd. de colunas do display
int lcdRows = 2;                                    // Qtd. de linhas do display
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);   // Cria o display de cristal



// Configuração do PID
double Kp = 2, Ki = 5, Kd = 1;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

// Instância dos sensores
TemperatureSensor temperatureSensor1(PIN_TEMPERATURE1, 0); // This is DS18B20
//TemperatureSensor temperatureSensor2(PIN_TEMPERATURE2, 1); // This is LM35
CO2Sensor co2sensor;

// Instância dos relays
Relay_system rele1(PIN_RELAY_1);
//Relay_system rele2(PIN_RELAY_2);

// configuração da wifi
std::vector<int> parsedIP = Publisher::parseIP(brokerURL);
IPAddress server(parsedIP[0],parsedIP[1],parsedIP[2],parsedIP[3]);
WiFiClient espClient;
PubSubClient client(espClient);
Publisher publisher;

void callback(char* topic, byte* payload, unsigned int length) {
    Serial.print("Mensagem recebida [");
    Serial.print(mqtt_topic);
    Serial.print("]: ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println();
}

void IRAM_ATTR zeroCross() {
    if (intensidade > 100) intensidade = 100;
    if (intensidade < 0) intensidade = 0;

    long t1 = 7500L * (100L - intensidade) / 100L;  // Ajuste do tempo para ESP32
    delayMicroseconds(t1);

    digitalWrite(PIN_DIM, HIGH);
    delayMicroseconds(10);  // Aumentando ligeiramente a largura do pulso
    digitalWrite(PIN_DIM, LOW);
    //printSensorData();


}

void updateTelemetryValues() {
    values.temperature1 = temperatureSensor1.getTemperature(); 
    // Serial.print(values.temperature1);
    // Serial.println();
    // // if(values.temperature1 > 30) {
    // //      rele1.powerOff();
    // // } else rele1.powerOn();

    // //values.temperature2 = temperatureSensor2.getTemperature(); 
    values.co2 = co2sensor.getCO2Concentration();
    //Serial.println(values.co2);
    values.resistence = intensidade;

}

void printSensorDataInitial() {
    lcd.init();                             // Inicia o objeto display
    lcd.backlight();                        // Coloca no modo blacklight
    lcd.setCursor(3, 0);                    // Coloca o cursos na col. 3 linha 0              
    lcd.print("Iniciando");                 // Imprime no display "Iniciando"
    lcd.setCursor(4, 1);                    // Coloca o cursor na col. 4 linha 1
    lcd.print("Silo Air");                  // Imprime no display "Silo Air"
    delay(10000);                           // Espera 10 segundos para ligar 
}


void printSensorData() {
    lcd.setCursor(0, 0);                    // Coloca o cursos na col. 3 linha 0
    lcd.clear();                            // Limpa a tela do display
    lcd.print("Temp: ");                    // Imprime no display "Temp:"
    lcd.print(values.temperature1);         // Imprime a temperatura em graus celsius
    lcd.print(" C");                        // Imprime a unidade
    lcd.setCursor(0, 1);                    // Coloca o cursor na col. 0 linha 1
    lcd.print("Pot: ");                     // Imprime no display "Pot:"
    lcd.print(intensidade);                 // 
    lcd.print(" %");                        // Imprime no display "%:"
    delay(2000);                            // Delay de 2s para limpar o display
    lcd.clear();                            // Limpa a tela do display
    lcd.setCursor(0, 0);                    // Coloca o cursos na col. 3 linha 0
    lcd.print("CO2: ");                     // Imprime no display "CO2:"
    lcd.print(values.co2);                  // Imprime o CO2 em ppm
    lcd.print(" ppm");                      // Imprime no display "ppm:"
}       

void setup() {      
    Serial.begin(BAUD_RATE);                // Configura o baude rate
    // Wire.begin();
    // printSensorDataInitial();               // Faz o print inicial
    // printSensorData();                      // Faz um print inicial da tela

    // Inicializa o CO2 Sensor
    co2sensor.initializeSensor(MQ135_PIN);  // Inicia o sensor de CO2  

    // Configura o PID
    Setpoint = 48.0;                        // Temperatura desejada
    myPID.SetMode(AUTOMATIC);               // Configura o PID no modo automatico
    myPID.SetOutputLimits(0, 30);           // Limita a saída do PID para 0 a 100


    xTaskCreatePinnedToCore(taskPID, "Task PID", 4096, NULL, 1, NULL, 1);
    rele1.powerOn();
    delay(5000);

    publisher.connectToWifi(client);
    client.setServer(server, brokerPort);
    client.setCallback(callback);

    pinMode(PIN_DIM, OUTPUT);
    pinMode(PIN_ZERO_CROSS, INPUT_PULLUP);

    attachInterrupt(digitalPinToInterrupt(PIN_ZERO_CROSS), zeroCross, FALLING);
    xTaskCreatePinnedToCore(taskMQTT, "Task MQTT", 4096, NULL, 1, NULL, 0);

}

void loop() {
    // O loop principal não precisa fazer nada
    // As tasks estão cuidando das operações
}

void taskMQTT(void* parameter) {
    while (1) {

        if (!client.connected()) {
            publisher.reconnect(client);
        }
        client.loop();
        publisher.sendData(client, values);


        delay(2500);  // Envia dados a cada 5 segundos
    }
}

void taskPID(void* parameter) {
    while (1) {        
        updateTelemetryValues();
        //printSensorData();
        // Atualiza o valor de entrada do PID com a temperatura lida
        Input = values.temperature1;
        // Calcula a saída do PID
        myPID.Compute();
        intensidade = Output;  // Ajusta a intensidade com a saída do PID
        delay(1000);
    }
}

#include "TemperatureSensor.h"
#include "Arduino.h"
#include "esp_adc_cal.h"
#include <OneWire.h>
#include <DallasTemperature.h>

TemperatureSensor::TemperatureSensor(int pin, int modeSensor) {
    this->pin = pin;
    this->modeSensor = modeSensor;

    // if (modeSensor == 1) {
    //     // Configure ADC width and attenuation for LM35
    //     adc1_config_width(ADC_WIDTH_BIT_12);
    //     adc1_config_channel_atten(getADC1Channel(this->pin), ADC_ATTEN_DB_11);

    //     adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
    //     esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, adc_chars);
    // }
}

float TemperatureSensor::getTemperature() {
    if (modeSensor == 0) {
        // Mode equal to 0, means that sensor is DS18B20
        OneWire oneWire(this->pin);
        DallasTemperature sensors(&oneWire);
        sensors.begin();
        sensors.requestTemperatures();
        float temperature = sensors.getTempCByIndex(0); 
        return temperature;
    }
    if (modeSensor == 1) {
        // // Mode equal to 1, means that sensor is LM35
        // const int numReadings = 10;
        // int readings[numReadings];
        // int total = 0;

        // for (int i = 0; i < numReadings; i++) {
        //     readings[i] = analogRead(this->pin);
        //     total += readings[i];
        //     delay(10);  // Small delay between readings
        // }

        // int averageReading = total / numReadings;
        // float voltage = readADC_Cal(averageReading);
        // float temperature = voltage / 10.0; // TempC = Voltage(mV) / 10

        // // Optional: Check for unreasonable jumps in temperature
        // if (temperature < -10 || temperature > 150) {
        //     return 0.0;  // Return 0.0 if temperature is out of reasonable bounds
        // }

        return 0;
    }
    return 0.0;  // Return 0.0 if not mode 0 or mode 1
}

// uint32_t TemperatureSensor::readADC_Cal(int ADC_Raw) {
//     return esp_adc_cal_raw_to_voltage(ADC_Raw, adc_chars);
// }

// // Helper function to get the ADC1 channel for a given GPIO pin
// adc1_channel_t TemperatureSensor::getADC1Channel(int pin) {
//     switch (pin) {
//         case 36: return ADC1_CHANNEL_0;
//         case 37: return ADC1_CHANNEL_1;
//         case 38: return ADC1_CHANNEL_2;
//         case 39: return ADC1_CHANNEL_3;
//         case 32: return ADC1_CHANNEL_4;
//         case 33: return ADC1_CHANNEL_5;
//         case 34: return ADC1_CHANNEL_6;
//         case 35: return ADC1_CHANNEL_7;
//         default: return ADC1_CHANNEL_0; // Default case, should be handled better
//     }
// }
